<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Auth\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/' , [PostController::class, 'index']);
Route::get('/posts/{id}/picture' , [PostController::class, 'get_pic']);

// Route::get('/test' , [RegisteredUserController::class, 'test']);
Route::get('/test' , [PostController::class, 'test']);


Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/posts' , [PostController::class, 'index_admin']);
    Route::get('/posts/create' , [PostController::class, 'create']);
    Route::post('/posts' , [PostController::class, 'store']);
    
    Route::get('/posts/{id}' , [PostController::class, 'edit']);
    Route::put('/posts/{id}' , [PostController::class, 'update']);
    Route::delete('/posts/{id}' , [PostController::class, 'destroy']);
});
    
Route::get('/user' , [UserController::class, 'show']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


