<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_admin()
    {
        $posts = Post::all();
        return view('admin.posts_index', ['posts' => $posts]);
    }


    public function test()
    {

        $date = Carbon::now()->format('Y-M');
        $dir = 'featured_pictures/'. $date .'/';
        return $dir;
        // Storage::disk('local')->put('example.txt', 'Contents asdjakhsdasuihdqwuik');
        // return Storage::download('1.png');
        // $contents = Storage::get('1.png');
        // return $contents;
        // return 'done';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderByDesc('id')->get();
        return view('index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $date = Carbon::now()->format('Y-M');
        try {
            $path = $request->file('featured_picture')->store('featured_pictures/'.$date. '/');
            $post = Post::create([
                'user_id' => $user->id,
                'title' => $request->title ,
                'post_text' =>  $request->post_text ,
                'featured_picture' =>  $path,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return 'مشکلی به وجود آمده!';
        }
        
        return ' پست با موفقیت ایجاد شد.';
    }

    /**
     * Display the Post featured picture.
     *
     */
    public function get_pic($id)
    {
        $post = Post::find($id);
        return Storage::download($post->featured_picture);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, $id)
    {
        $post = Post::find($id);
        return view('admin.posts_edit', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post , $id)
    {
        //
        $post = Post::find($id);
        $post->title = $request->title;
        $post->post_text = $request->post_text;
        $post->save();

        return "با موفقیت ویرایش شد.";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post , $id)
    {
        $post = Post::find($id);
        $post->delete();

        return "پست با موفقیت حذف شد.";
    }
}
