<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Post;


class UserController extends Controller
{

    /**
     * Show the profile for a given user.
     *
     */
    public function show()
    {
        // $users = DB::select('select * from users');
        // $users = DB::table('users')->orderBy('password')->get();
        // $users = DB::table('users')->get();
        // $users = User::where('name', 'mahdi')->get();
        return $users;
    }


    public function register(Request $request)
    {
        // $input = $request->all();
        // return $input;
        // DB::insert('insert into users (name,email,password) values (?,?,?)'
        //             , [$input['name'], $input['email'], $input['password'] ]);
        
        // $user = new User;
        // $user->name = $request->name;
        // $user->email = $request->email;
        // $user->password = $request->password;
        // $user->save();

        $user = User::create([
            'name' => $request->name ,
            'email' => $request->email ,
            'password' => $request->password,
        ]);

        return "کاربر با موفقیت ایجاد شد." ;
    }

    public function update(Request $request)
    {
        // $input = $request->all();
        // $affected = DB::update(
        //     'update users set name = ? where id = ?',
        //     [$input['name'] , $input['id']]
        // );

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->save();

        return $user;
    }

    public function delete(Request $request)
    {
        // $input = $request->all();
        // $deleted = DB::delete('delete from users where id = ?' , [$input['id']]);

        // $user = User::find($request->id);
        // $user->delete();

        User::find($request->id)->delete();

        return 'کاربر حذف شد';
    }


}
